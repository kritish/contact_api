<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::find($request->user())->first();
        if (!$user) {
            return response()->json(["error" => "not authorized"], 401);
        }

        //
        $contacts = $user->contacts;
        return response()->json(["contacts" => $contacts], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->user())->first();

        if (!$user) {
            return response()->json(["error" => 'not authorized'], 401);
        }
        if (!$request->contact_name || !$request->contact_no) {
            return response()->json(["error" => "contact name or no. is not supplied"], 400);
        }

        $user->contacts()->save(new Contact(['contact_name' => $request->contact_name, 'contact_no' => $request->contact_no]));
        return response()->json(['message' => 'new contact created'], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $contact = Contact::find($id);
        if (!$contact) {
            return response()->json(['errors' => 'Contact not found'], 404);
        }
        // named response is good practice
        return response()->json(['contact' => $contact], 200);
        // return $contact;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (!$request->contact_name || !$request->contact_no) {
            return resonse()->json(['error' => 'contact name or contact no. is not supplied'], 400);
        }
        $user = User::find($request->user())->first();

        if (!$user) {
            return response()->json(['error' => 'not authorized'], 401);
        }

        $user->contacts()->whereId($id)->update(["contact_name" => $request->contact_name, "contact_no" => $request->contact_no]);
        return response()->json(["message" => "contact updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contact = Contact::find($id);
        $contact->delete();
        echo 'delete success';
    }
}
