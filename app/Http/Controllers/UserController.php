<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        return response()->json(['users' => $users], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //  signup endpoint
    public function store(Request $request)
    {
        if (!$request->email || !$request->password) {
            return response()->json(["error" => "email or password is empty"], 400);
        }
        $user = new User();
        $emailExists = User::where('email', $request->email)->first();

        if ($emailExists) {
            return response()->json(['error' => 'email already registered'], 400);
        }
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['message' => 'new user crated', 'new_user' => $user], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  login

    //
    public function show($id)
    {
        //
    }

    public function login(Request $request)
    {
        // get data from $_REQUEST
        $email = $request->email;
        $password = $request->password;
        if (!$email || !$password) {
            return response()->json(["error" => "email or password not supplied"], 400);
        }

        // check if user exists
        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json(['error' => 'user not found'], 404);
        }
        // check if password matches
        if (!Hash::check($password, $user->password)) {
            return response()->json(['error' => 'email or password invalid'], 400);
        }
        // generate and send token
        $token = $user->createToken('token-name');
        return response()->json(['token' => 'Bearer ' . $token->plainTextToken]);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json(["message" => "logout success"], 200);
    }

    public function test(Request $request)
    {
        return $request->user();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
