<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

use App\Http\Controllers\ContactController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// contact end point
Route::middleware('auth:sanctum')->get('contacts', [ContactController::class, 'index']);
Route::middleware('auth:sanctum')->post('contacts', [ContactController::class, 'store']);
Route::middleware('auth:sanctum')->get('contacts/{id}', [ContactController::class, 'show']);
Route::middleware('auth:sanctum')->post('contacts/{id}', [ContactController::class, 'update']);
Route::middleware('auth:sanctum')->delete('contacts/{id}', [ContactController::class, 'destroy']);

//
// user end points
//
// get all users
Route::get('users', [UserController::class, 'index']);
// signup
Route::post('users', [UserController::class, 'store']);
// login
Route::post('users/login', [UserController::class, 'login']);
// logout
Route::middleware('auth:sanctum')->get('users/logout', [UserController::class, 'logout']);
// protected route
Route::middleware('auth:sanctum')->get('users/test', [UserController::class, 'test']);
